require 'test_helper'

class Admin::PermissionsControllerTest < ActionController::TestCase
  setup do
    @admin_permission = admin_permissions(:one)

    sign_in FactoryGirl.create(:admin)
  end

  test 'should get grid' do
    get :grid
    assert_response :success
  end

  test 'should update permissions' do
    post :update_grid
    assert_redirected_to admin_permissions_path
  end
end
